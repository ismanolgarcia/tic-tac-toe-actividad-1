<<<<<<< HEAD
<h1 align="center">Tic tac toe</h1>
<p>Proyecto creado para la tarea de infotep correspondiente a la actividad numero 1 del modulo 3 </p>

<p align="center">
  <a href="#-project">Proyecto</a> |
  <a href="#-technologies">Tecnologia</a> |
  <a href="#ℹ%EF%B8%8F-how-to-use">Como usarlo</a> |  
  <a href="https://fellipeutaka-tictactoe.vercel.app">Live demo</a>
</p>

## 💻 Project

Un juego creado con React y Tailwind CSS

<h1 align="center">
  <img alt="Preview" src=".github/preview.png" width="500px" />
</h1>

## 🚀 Tecnologia utilizadas

Este proyecto estuvo desarrollado con las siguentes tecnologias

- [TypeSc9ript](ts)
- [React](react)
- [Vite](vite])
- [Vitest](vitest)
- [Tailwind](tailwind)

## ℹ️ Como usarlo o descargar el codigo

Solo tiene que seguir los comandos que necesite segun las herramientas de desarrollo que utilice.

Pero como requisitos tienes que tener instalado nodejs y git

From your command line:

```bash
# Clone this repository
$ git clone https://github.com/fellipeutaka/tic-tac-toe.git

# Install dependencies
# If you are using NPM
$ npm install

# If you are using Yarn
$ yarn install

# If you are using PNPM (current package manager)
$ pnpm install

# Start server
# If you are using NPM
$ npm run dev

# If you are using Yarn
$ yarn dev

# If you are using PNPM (current package manager)
$ pnpm dev
```
